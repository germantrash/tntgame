package de.tntgame.launcher;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TNTGLauncher extends ApplicationAdapter {
	private final String TAG = "[" + getClass().getSimpleName() + "]: ";

	private SpriteBatch batch;
	private Texture img;
	
	@Override
	public void create () {
		System.out.println(TAG + ">>> Launching the game...");

		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");

		System.out.println(TAG + ">>> Ended the start process.");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}
}