package de.tntgame.launcher.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.tntgame.launcher.TNTGLauncher;

public class DesktopLauncher {
	private static final String TAG = "[DesktopLauncher]: ";

	public static void main (String[] arg) {
		System.out.println(TAG + "Starting the LwjglApplication...");

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "TNTGame";

		new LwjglApplication(new TNTGLauncher(), config);
	}
}
