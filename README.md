# README #
Integrate the app in Intellij IDEA via VCS.

1. Go to VCS -> Checkout from Version Contol -> Git
2. Fill in the correct data
3. Do a git-pull request.

### What is this repository for? ###

* This repo is for the TNTGame. It's made with libGDX

### The main game... ###

* ... is build on top of the libGDX platform and uses Box2D and other cool libGDX extensions.
* ... is going to get great!